# responsive-layout-challenge

## Challenge a

<div align="center">
<img src="img/1a.png">
<p><strong>Figure:</strong>Desktop Version</p>
</div>

---

<div align="center">
<img src="img/1b.png">
<p><strong>Figure:</strong>Tablet Version</p>
</div>

---

<div align="center">
<img src="img/1c.png">
<p><strong>Figure:</strong>Mobile Version</p>
</div>

---

## Challenge b

<div align="center">
<img src="img/2a.png">
<p><strong>Figure:</strong>Desktop Version</p>
</div>

---

</div>
<div align="center">
<img src="img/2b.png">
<p><strong>Figure:</strong>Tablet Version</p>
</div>

---

<div align="center">
<img src="img/2c.png">
<p><strong>Figure:</strong>Mobile Version</p>
</div>
